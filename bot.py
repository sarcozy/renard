#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2017, Jean-Benoist Leger <jb@leger.tf>
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
# 
#     Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in
#     the documentation and/or other materials provided with the
#     distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from twisted.words.protocols import irc
from twisted.internet import reactor, protocol
from twisted.python import log

# system imports

import chiffre
import time,sys

import configobj
import re
import numpy as np

import random

def phrases(what):
    if what == 'val':
        return random.choice([
            u'Ce calcul est incorrect.',
            u'FAUX.',
            u'Il faut apprendre à compter.',
            u'Mais bien sûr, et 1+1=3.',
            u'Voyons voir… non, c\'est faux.',
            u'C\'est bien, c\'est un calcul. Maintenant faudrait que ça donne la bonne valeur.',
            u'Rajoute 12 pour voir.',
            u'Divise par 7, ça devrait donner la bonne valeur, au point où on en est.',
            u'Un peu d\'efforts, le calcul c\'est du programme de primaire',
            ]).encode('utf-8')
    if what == 'nbs':
        return random.choice([
            u'Seules les valeurs du tirage peuvent être utilisées. Chaque valeur ne peut être utilisée plus d\'une fois',
            ]).encode('utf-8')

class MessageLogger:
    
    def log(self, message):
        print message

class MmBot(irc.IRCClient):
    
    def __init__(self,botconf):
        self.botconf = botconf
        self.nickname = self.botconf['nickname']
        self.parties = {}
        self.averti = {}
        self.ignore = {}
        self.erreur = {}
        
    def connectionMade(self):
        irc.IRCClient.connectionMade(self)
        self.logger = MessageLogger()
        self.logger.log("[connected at %s]" % 
                        time.asctime(time.localtime(time.time())))

    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)
        self.logger.log("[disconnected at %s]" % 
                        time.asctime(time.localtime(time.time())))

    def signedOn(self):
        """Called when bot has succesfully signed on to server."""
        for c in self.botconf['chans']:
            self.logger.log("[asked to join %s]" % c)
            self.join(c)

    def kickedFrom(self, channel, kicker, message):
        self.logger.log("[kicked from %s by %s at %s]" % 
                ( channel, kicker,
                        time.asctime(time.localtime(time.time())))
                )

    def joined(self, channel):
        """This will get called when the bot joins the channel."""
        self.logger.log("[I have joined %s]" % channel)

    def c_help(self, user, channel):
        rep = u"Jeu dont l'objectif est de construire le nombre demandé à partir\n"
        rep+= u"   d'opérations artimétiques simples (+-*/) et des nombres proposés\n"
        rep+= u"   en utilisant au plus une fois chaque nombre\n"
        rep+= u"Les commandes du jeu sont :\n"
        rep+= u"  - jouer (variante: encore)\n"
        rep+= u"  - indice (variante: hint)\n"
        rep+= u"  - solution (variante: tu fais chier)\n"
        rep+= u"  - ignore\n"
        rep+= u"  - scores\n"
        rep+= u"  - help (variante: motoculteur)\n"
        rep+= u"  - join\n"
        rep+= u"  - part\n"
        self.msg(user, rep.encode('utf8'))

    def c_jouer(self, user, channel):
        if channel in self.parties:
            self.msg(channel, "%s: Partie en cours\nRappel:" % s)
            self.c_affiche_partie(channel)
            return None
        
        self.botconf.reload()
        if not 'hardness' in self.botconf:
            self.botconf['hardness'] = 2
        self.parties[channel] = [chiffre.genjeu(int(self.botconf['hardness'])), False, 0]
        self.parties[channel][2] = time.time()
        self.c_affiche_partie(channel)
        if not channel in self.ignore:
            self.ignore[channel] = {}
        ignored = [u for u in self.ignore[channel].keys()]
        for k in ignored:
            self.ignore[channel][k] -= 1
            if self.ignore[channel][k] == 0:
                del self.ignore[channel][k]
        self.averti[channel] = []
        self.erreur[channel] = {}

    def c_affiche_partie(self,channel):
        rep = "objectif: %i\n" % self.parties[channel][0][1]
        rep+= "tirage: %s\n" % ', '.join(["%i" % i for i in self.parties[channel][0][0]])
        if self.parties[channel][1]:
            rep+= "indice: %s\n" % re.sub('[0-9]+','_',self.parties[channel][0][2])
        self.msg(channel, rep)

    def c_indice(self,user,channel):
        if user in self.ignore[channel]:
            return None
        if channel not in self.parties:
            return None
        if self.parties[channel][1]:
            return None
        self.parties[channel][1] = True
        self.c_affiche_partie(channel)

    def c_solution(self,user,channel):
        if user in self.ignore[channel]:
            return None
        self.c_affiche_partie(channel)
        rep = "solution: %s\n" % self.parties[channel][0][2]
        self.msg(channel, rep)
        del self.parties[channel]

    def c_scores(self,user,channel):
        self.botconf.reload()
        scores = self.botconf['scores']
        joueurs = scores.keys()
        joueurs.sort(key=lambda x: -int(scores[x]))
        rep = ""
        for k in joueurs:
            rep += "%s: %s, " % (k,scores[k])
        self.msg(user,rep)

    def c_ignore(self, user, channel, smbdy):
        if not self.botconf['ignore'] == '1':
            return None
        if channel in self.parties:
            if not smbdy in self.ignore[channel]:
                self.ignore[channel][smbdy] = 1
                self.msg(channel, "%s: %s sera ignoré pour cette partie" % (user, smbdy))
                self.averti[channel].append(user)

    def c_plainte(self, user, channel):
        if channel in self.parties:
            if user in self.averti[channel]:
                self.msg(channel,'%s: Et tu me réponds en plus ?' % user)
                self.msg(channel,'%s: Cette partie est terminée.' % user)
                if not user in self.ignore[channel]:
                    self.ignore[channel][user]=2
                else:
                    self.ignore[channel][user]+=2
                self.msg(channel,'%s: Et tu seras ignoré pour %d parties.' %
                        (user, self.ignore[channel][user]-1))
                del self.parties[channel]

    def c_proposition(self,user,channel, msg):
        if channel not in self.parties:
            return None
        if user in self.ignore[channel]:
            return None
        self.logger.log("ignore: %s" %  self.ignore[channel])
        tirage, valeur, reponse = self.parties[channel][0]
        print tirage
        print valeur
        print msg
        res, what = chiffre.isvalidgame(tirage, valeur, msg)
        if not res:
            prejudice = False
            if what == 'val':
                self.msg(channel, ('%s: ' % user ) + phrases('val'))
                self.averti[channel].append(user)
                if not user in self.erreur[channel]:
                    self.erreur[channel][user] = 0
                prejudice = True
            if what == 'nbs':
                self.msg(channel, ('%s: ' % user ) + phrases('nbs'))
                self.averti[channel].append(user)
                if not user in self.erreur[channel]:
                    self.erreur[channel][user] = 0
                prejudice = True
            if prejudice:
                self.erreur[channel][user]+=1
                perte = 7**(self.erreur[channel][user]-1)
                self.c_update_score(user,-perte)
                self.msg(channel,'%s: Tu viens de perdre %d pts.' % (user,perte))
            return None
        dt = time.time() - self.parties[channel][2]
        sc = 40*np.exp(-dt/60)+10
        if self.parties[channel][1]:
            sc/=4
        sc = int(sc+.5)
        self.c_update_score(user, sc)
        self.msg(channel,'%s: Le compte est bon. Points: %i' % (user, sc))
        del self.parties[channel]

    def c_update_score(self, user, sc):
        self.botconf.reload()
        if not user in self.botconf['scores']:
            self.botconf['scores'][user] = 0
        self.botconf['scores'][user] = int(self.botconf['scores'][user]) + sc
        if self.botconf['scores'][user] < 0:
            del self.botconf['scores'][user]
        self.botconf.write()

    def privmsg(self, user, channel, msg):
        """This will get called when the bot receives a message."""
        user = user.split('!', 1)[0]
        self.logger.log("%s: <%s> %s" % (channel, user, msg))
        
        if channel == self.nickname:
            if re.match('help',msg) or re.match('motoculteur',msg):
                self.c_help(user,channel)
                return None
            a=re.match('join (#[A-Za-z0-9-]+)', msg)
            if a:
                self.join(a.groups()[0])
                return None

            self.msg(user,"Toto passoir mange du lapin")
            return None

        a=re.match(self.nickname+': (.*)',msg)
        if a:
            m=a.groups()[0]

            if re.match('help',m) or re.match('motoculteur',m):
                self.c_help(user,channel)
            elif re.match('jouer',m) or re.match('encore',m):
                self.c_jouer(user,channel)
            elif re.match('indice',m) or re.match('hint',m):
                self.c_indice(user,channel)
            elif re.match('solution',m) or re.match('tu fais chier',m):
                self.c_solution(user,channel)
            elif re.match('scores',m):
                self.c_scores(user,channel)
            elif re.match('part',m) or re.match('casse toi pauvre con',m):
                self.part(channel)
            else:
                a=re.match('ignore (\S+)', m)
                if a:
                    self.c_ignore(user, channel, a.groups()[0])
                else:
                    a=re.match('join (#[A-Za-z0-9-]+)', m)
                    if a:
                        self.join(a.groups()[0])
                    else:
                        m = re.sub(self.nickname+' ','',m)
                        if re.match('[a-z]+', m, flags = re.I):
                            self.c_plainte(user,channel)
                        else:
                            self.c_proposition(user,channel,m)

class MmBotFactory(protocol.ClientFactory):

    def __init__(self,botconf):
        self.botconf = botconf
        pass

    def buildProtocol(self, addr):
        p = MmBot(self.botconf)
        p.factory = self
        return p

    def clientConnectionLost(self, connector, reason):
        time.sleep(30)
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print "connection failed:", reason
        time.sleep(30)
        connector.connect()


if __name__ == '__main__':

    botconf = configobj.ConfigObj('bot.conf')
    log.startLogging(sys.stdout)
    f = MmBotFactory(botconf)

    reactor.connectTCP(botconf['server_name'], int(botconf['server_port']), f)
    reactor.run()
